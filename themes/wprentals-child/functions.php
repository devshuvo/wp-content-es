<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;


if ( !function_exists( 'wpestate_chld_thm_cfg_parent_css' ) ):
    function wpestate_chld_thm_cfg_parent_css() {

        wp_enqueue_style('select2', get_stylesheet_directory_uri().'/css/select2.min.css');
        wp_enqueue_script('select2', get_stylesheet_directory_uri().'/js/select2.min.js', array('jquery'));


        $parent_style = 'wpestate_style';
        wp_enqueue_style('bootstrap',get_template_directory_uri().'/css/bootstrap.css', array(), '1.0', 'all');
        wp_enqueue_style('bootstrap-theme',get_template_directory_uri().'/css/bootstrap-theme.css', array(), '1.0', 'all');
        wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css',array('bootstrap','bootstrap-theme'),'all' );


        wp_enqueue_script('wp-rentals-child', get_stylesheet_directory_uri().'/js/wp-rentals-child.js', array('jquery'));

        wp_enqueue_style( 'wpestate-child-style',
            get_stylesheet_directory_uri() . '/style.css',
            array( $parent_style ),
            wp_get_theme()->get('Version')
        );
    }

endif;
add_action( 'wp_enqueue_scripts', 'wpestate_chld_thm_cfg_parent_css' );
load_child_theme_textdomain('wprentals', get_stylesheet_directory().'/languages');
// END ENQUEUE PARENT ACTION

add_action( 'wp_enqueue_scripts', 'wpestate_chld_thm_override_scripts' );
if ( !function_exists( 'wpestate_chld_thm_override_scripts' ) ):
    function wpestate_chld_thm_override_scripts() {

       wp_dequeue_script('wpestate_ajaxcalls_add');
       wp_enqueue_script('wpestate_ajaxcalls_add', trailingslashit( get_stylesheet_directory_uri() ).'js/ajaxcalls_add.js',array('jquery'), '1.0', true);

       wp_dequeue_script('wpestate_ajaxcalls');
       wp_enqueue_script('wpestate_ajaxcalls', trailingslashit( get_stylesheet_directory_uri() ).'js/ajaxcalls.js',array('jquery'), '1.0', true);

       wp_dequeue_script('wpestate_control');
       wp_enqueue_script('wpestate_control', trailingslashit( get_stylesheet_directory_uri() ).'js/control.js',array('jquery'), '1.0', true);
    }
endif;


add_filter( 'wp_dropdown_cats', 'wp_dropdown_cats_multiple', 10, 2 );

function wp_dropdown_cats_multiple( $output, $r ) {

    if( isset( $r['multiple'] ) && $r['multiple'] ) {

         $output = preg_replace( '/^<select/i', '<select multiple', $output );

        $output = str_replace( "name='{$r['name']}'", "name='{$r['name']}[]'", $output );

        foreach ( array_map( 'trim', explode( ",", $r['selected'] ) ) as $value )
            $output = str_replace( "value=\"{$value}\"", "value=\"{$value}\" selected", $output );

    }

    return $output;
}

/**
 * [hide_this description]
 * @return [type] [description]
 */
function hide_this(){
    return false;
}

/**
 * Get taxonomies terms links.
 *
 * @see get_object_taxonomies()
 */
function taxonomies_selected_val( $post_id, $taxonomy ) {

    // Get post type taxonomies.
    $taxonomies = get_the_terms( $post_id, $taxonomy );

    $out = array();

    foreach ( $taxonomies as $terms ){

        $out[] = $terms->term_id;

    }

    return implode( ',', $out );
}


function before_search_map_filter_function(){

    $provincia_selected = ( $_GET['provincia'] ) ? $_GET['provincia'] : '';
    $usos_selected = ( $_GET['usos'] ) ? $_GET['usos'] : '';
    $localizaciones_selected = ( $_GET['localizaciones'] ) ? $_GET['localizaciones'] : '';

    ob_start(); ?>

    <div class="top-search-div">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <div class="select-input">
                        <?php
                        $args = array(
                            'class'       => 'select-submit2',
                            'hide_empty'  => 1,
                            'selected'    => $provincia_selected,
                            'name'        => 'provincia_category_select',
                            'id'          => 'provincia_category_select',
                            'orderby'     => 'NAME',
                            'order'       => 'ASC',
                            'show_count'       => 1,
                            'show_option_none'   => esc_html__( 'Provincia','wprentals'),
                            'taxonomy'    => 'provincia',
                            'hierarchical'=> true,
                            'value_field'       => 'slug',
                        );
                        wp_dropdown_categories( $args ); ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="select-input">
                        <?php
                        $args = array(
                            'class'       => 'select-submit2',
                            'hide_empty'  => 1,
                            'selected'    => $usos_selected,
                            'name'        => 'usos_category_select',
                            'id'          => 'usos_category_select',
                            'orderby'     => 'NAME',
                            'order'       => 'ASC',
                            'show_count'       => 1,
                            'show_option_none'   => esc_html__( 'Usos','wprentals'),
                            'taxonomy'    => 'usos',
                            'hierarchical'=> true,
                            'value_field'       => 'slug',
                        );
                        wp_dropdown_categories( $args ); ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="select-input">
                        <?php
                        $args = array(
                            'class'       => 'select-submit2',
                            'hide_empty'  => 1,
                            'selected'    => $localizaciones_selected,
                            'name'        => 'localizaciones_category_select',
                            'id'          => 'localizaciones_category_select',
                            'orderby'     => 'NAME',
                            'order'       => 'ASC',
                            'show_count'       => 1,
                            'show_option_none'   => esc_html__( 'Localizaciones','wprentals'),
                            'taxonomy'    => 'localizaciones',
                            'hierarchical'=> true,
                            'value_field'       => 'slug',
                        );
                        wp_dropdown_categories( $args ); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    $output =  ob_get_clean();

    echo $output;

}
add_action( 'before_search_map_filter', 'before_search_map_filter_function' );



if ( !function_exists('wp_terms_checbox') ) {
    function wp_terms_checbox( $args = array() ){

        $defaults = array(
            'taxonomy'             => 'category',
            'echo'                 => true,
        );

        $parsed_args = wp_parse_args( $args, $defaults );
        $taxonomy = $parsed_args['taxonomy'];

        $get_terms = get_terms( array(
            'taxonomy' => $taxonomy,
            'hide_empty' => false,
        ) );

        $output = '';

        if ( $get_terms && !is_wp_error( $get_terms ) ) :
            foreach( $get_terms as $term ) :
                echo "<option value='<?php echo esc_attr( $term->term_id ); ?>''><?php echo esc_html( $term->name ); ?></option>";
                //$output .= "<li id='{$taxonomy}-{$term->term_id}'><label class='selectit'><input value='279' type="checkbox" name="tax_input[servicios][]" id="in-servicios-279"> another</label></li>";
            endforeach;
       endif;
    }
}

function dev_terms_checklist( $post_id = 0, $args = array() ) {
    $defaults = array(
        'descendants_and_self' => 0,
        'selected_cats'        => false,
        'popular_cats'         => false,
        'walker'               => new Dev_Walker_Category_Checklist,
        'taxonomy'             => 'category',
        'checked_ontop'        => true,
        'echo'                 => true,
    );

    /**
     * Filters the taxonomy terms checklist arguments.
     *
     * @since 3.4.0
     *
     * @see wp_terms_checklist()
     *
     * @param array $args    An array of arguments.
     * @param int   $post_id The post ID.
     */
    $params = apply_filters( 'wp_terms_checklist_args', $args, $post_id );

    $parsed_args = wp_parse_args( $params, $defaults );

    if ( empty( $parsed_args['walker'] ) || ! ( $parsed_args['walker'] instanceof Walker ) ) {
        $walker = new Walker_Category_Checklist;
    } else {
        $walker = $parsed_args['walker'];
    }

    $taxonomy             = $parsed_args['taxonomy'];
    $descendants_and_self = (int) $parsed_args['descendants_and_self'];

    $args = array( 'taxonomy' => $taxonomy );

    $tax              = get_taxonomy( $taxonomy );
    $args['disabled'] = ! current_user_can( $tax->cap->assign_terms );

    $args['list_only'] = ! empty( $parsed_args['list_only'] );

    if ( is_array( $parsed_args['selected_cats'] ) ) {
        $args['selected_cats'] = $parsed_args['selected_cats'];
    } elseif ( $post_id ) {
        $args['selected_cats'] = wp_get_object_terms( $post_id, $taxonomy, array_merge( $args, array( 'fields' => 'ids' ) ) );
    } else {
        $args['selected_cats'] = array();
    }

    if ( is_array( $parsed_args['popular_cats'] ) ) {
        $args['popular_cats'] = $parsed_args['popular_cats'];
    } else {
        $args['popular_cats'] = get_terms(
            array(
                'taxonomy'     => $taxonomy,
                'fields'       => 'ids',
                'orderby'      => 'count',
                'order'        => 'DESC',
                'number'       => 10,
                'hierarchical' => false,
            )
        );
    }

    $categories = (array) get_terms(
        array(
            'taxonomy'     => $taxonomy,
            'hide_empty'   => true,
        )
    );

    $output = '';

    if ( $parsed_args['checked_ontop'] ) {
        // Post-process $categories rather than adding an exclude to the get_terms() query
        // to keep the query the same across all posts (for any query cache).
        $checked_categories = array();
        $keys               = array_keys( $categories );

        foreach ( $keys as $k ) {
            if ( in_array( $categories[ $k ]->term_id, $args['selected_cats'] ) ) {
                $checked_categories[] = $categories[ $k ];
                unset( $categories[ $k ] );
            }
        }

        // Put checked categories on top.
        $output .= $walker->walk( $checked_categories, 0, $args );
    }
    // Then the rest of them.
    $output .= $walker->walk( $categories, 0, $args );

    if ( $parsed_args['echo'] ) {
        echo $output;
    }

    return $output;
}

/**
 *
 */
class Dev_Walker_Category_Checklist extends Walker {
    public $tree_type = 'category';
    public $db_fields = array(
        'parent' => 'parent',
        'id'     => 'term_id',
    ); // TODO: Decouple this.

    /**
     * Starts the list before the elements are added.
     *
     * @see Walker:start_lvl()
     *
     * @since 2.5.1
     *
     * @param string $output Used to append additional content (passed by reference).
     * @param int    $depth  Depth of category. Used for tab indentation.
     * @param array  $args   An array of arguments. @see wp_terms_checklist()
     */
    public function start_lvl( &$output, $depth = 0, $args = array() ) {
        $indent  = str_repeat( "\t", $depth );
        $output .= "$indent<ul class='children'>\n";
    }

    /**
     * Ends the list of after the elements are added.
     *
     * @see Walker::end_lvl()
     *
     * @since 2.5.1
     *
     * @param string $output Used to append additional content (passed by reference).
     * @param int    $depth  Depth of category. Used for tab indentation.
     * @param array  $args   An array of arguments. @see wp_terms_checklist()
     */
    public function end_lvl( &$output, $depth = 0, $args = array() ) {
        $indent  = str_repeat( "\t", $depth );
        $output .= "$indent</ul>\n";
    }

    /**
     * Start the element output.
     *
     * @see Walker::start_el()
     *
     * @since 2.5.1
     *
     * @param string $output   Used to append additional content (passed by reference).
     * @param object $category The current term object.
     * @param int    $depth    Depth of the term in reference to parents. Default 0.
     * @param array  $args     An array of arguments. @see wp_terms_checklist()
     * @param int    $id       ID of the current term.
     */
    public function start_el( &$output, $category, $depth = 0, $args = array(), $id = 0 ) {
        if ( empty( $args['taxonomy'] ) ) {
            $taxonomy = 'category';
        } else {
            $taxonomy = $args['taxonomy'];
        }

        if ( 'category' === $taxonomy ) {
            $name = 'post_category';
        } else {
            $name = 'tax_input[' . $taxonomy . ']';
        }

        $args['popular_cats'] = empty( $args['popular_cats'] ) ? array() : $args['popular_cats'];
        $class                = in_array( $category->term_id, $args['popular_cats'] ) ? ' class="popular-category"' : '';

        $args['selected_cats'] = empty( $args['selected_cats'] ) ? array() : $args['selected_cats'];

        if ( ! empty( $args['list_only'] ) ) {
            $aria_checked = 'false';
            $inner_class  = 'category';

            if ( in_array( $category->term_id, $args['selected_cats'] ) ) {
                $inner_class .= ' selected';
                $aria_checked = 'true';
            }

            $output .= "\n" . '<li' . $class . '>' .
                '<div class="' . $inner_class . '" data-term-id=' . $category->term_id .
                ' tabindex="0" role="checkbox" aria-checked="' . $aria_checked . '">' .
                /** This filter is documented in wp-includes/category-template.php */
                esc_html( apply_filters( 'the_category', $category->name, '', '' ) ) . '</div>';
        } else {
            $output .= "\n<li id='{$taxonomy}-{$category->term_id}'$class>" .
                '<label class="selectit"><input value="' . $category->term_id . '" type="checkbox" name="' . $name . '[]" id="in-' . $taxonomy . '-' . $category->term_id . '"' .
                checked( in_array( $category->term_id, $args['selected_cats'] ), true, false ) .
                disabled( empty( $args['disabled'] ), false, false ) . ' /> ' .
                /** This filter is documented in wp-includes/category-template.php */
                esc_html( apply_filters( 'the_category', $category->name, '', '' ) ) . '<span class="count"> ('.$category->count.')</span></label>';
        }
    }

    /**
     * Ends the element output, if needed.
     *
     * @see Walker::end_el()
     *
     * @since 2.5.1
     *
     * @param string $output   Used to append additional content (passed by reference).
     * @param object $category The current term object.
     * @param int    $depth    Depth of the term in reference to parents. Default 0.
     * @param array  $args     An array of arguments. @see wp_terms_checklist()
     */
    public function end_el( &$output, $category, $depth = 0, $args = array() ) {
        $output .= "</li>\n";
    }
}

/**
 * remove_comma_set_array
 */
function remove_comma_set_array( $value ){
    // Comma to array
    $value = explode( ',', $value );

    // Remove empty array
    $value = array_filter( $value );

    return $value;
}

function custom_admin_scripts(){
    ob_start(); ?>

<style>
#estate_property-sectionid label[for=property_state],
#estate_property-sectionid input#property_state,
#estate_property-sectionid label[for=property_county],
#estate_property-sectionid input#property_county {
  display: none;
}
</style>
    <?php
    $output = ob_get_clean();

    echo $output;

}
add_action( 'admin_head', 'custom_admin_scripts', 10 );

function dev_wp_rental_extra_meta_function( $post_id ) {
    echo $post_id;
    $capacidad = get_post_meta( $post_id, 'property_bedrooms', true );
    $superficie = get_post_meta( $post_id, 'property_size', true );
    $salas = get_post_meta( $post_id, 'property_rooms', true );

    ob_start(); ?>
    <div class="prop_extra_metas">
        <ul>
            <?php if( $capacidad ) : ?>
                <li>
                    <div class="icon">
                        <?php echo dev_get_svg('man'); ?>
                    </div>
                    <div class="text"><?php esc_html_e( $capacidad ); ?></div>
                </li>
            <?php endif; ?>

            <?php if( $superficie ) : ?>
                <li>
                    <div class="icon">
                        <?php echo dev_get_svg('move'); ?>
                    </div>
                    <div class="text"><?php esc_html_e( $superficie ); ?><span>m2</span></div>
                </li>
            <?php endif; ?>

            <?php if( $salas ) : ?>
                <li>
                    <div class="icon">
                        <?php echo dev_get_svg('square'); ?>
                    </div>
                    <div class="text"><?php esc_html_e( $salas ); ?></div>
                </li>
            <?php endif; ?>
        </ul>
    </div>

    <?php
    $output = ob_get_clean();

    echo  $output;

}
add_action( 'dev_rental_extra_metas', 'dev_wp_rental_extra_meta_function' );

/**
 *  SVG Icons
 *
 * @return  string
 */
if ( ! function_exists('dev_get_svg') ) {
    function dev_get_svg( $icon = null ){

        if ( ! $icon ) {
            return;
        }

        switch ( $icon ) {

                case 'man':
                    $output ='<?xml version="1.0" encoding="iso-8859-1"?> <!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  --> <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"> <g> <g> <g> <circle cx="256" cy="51.2" r="51.2"/> <path d="M341.333,119.467H170.667c-23.564,0-42.667,19.103-42.667,42.667v128c0,14.138,11.461,25.6,25.6,25.6 c14.138,0,25.6-11.461,25.6-25.6V179.2h17.067v307.2c0,14.138,11.462,25.6,25.6,25.6s25.6-11.461,25.6-25.6V315.733h17.067V486.4 c0,14.138,11.462,25.6,25.6,25.6s25.6-11.461,25.6-25.6V179.2H332.8v110.933c0,14.138,11.462,25.6,25.6,25.6 s25.6-11.461,25.6-25.6v-128C384,138.569,364.898,119.467,341.333,119.467z"/> </g> </g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> </svg>';
                            break;

                case 'move':
                    $output ='<?xml version="1.0" encoding="iso-8859-1"?> <!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  --> <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"> <g> <g> <path d="M508.875,248.458l-74.667-74.667c-3.042-3.052-7.646-3.948-11.625-2.313c-3.979,1.646-6.583,5.542-6.583,9.854v32 c0,2.833,1.125,5.542,3.125,7.542l13.792,13.792H277.333V79.083l13.792,13.792c2,2,4.708,3.125,7.542,3.125h32 c4.313,0,8.208-2.594,9.854-6.583c1.646-3.99,0.729-8.573-2.313-11.625L263.542,3.125c-4.167-4.167-10.917-4.167-15.083,0 l-74.667,74.667c-3.042,3.052-3.958,7.635-2.313,11.625c1.646,3.99,5.542,6.583,9.854,6.583h32c2.833,0,5.542-1.125,7.542-3.125 l13.792-13.792v155.583H79.083l13.792-13.792c2-2,3.125-4.708,3.125-7.542v-32c0-4.313-2.604-8.208-6.583-9.854 c-3.958-1.656-8.583-0.75-11.625,2.313L3.125,248.458c-4.167,4.167-4.167,10.917,0,15.083l74.667,74.667 c2.042,2.042,4.771,3.125,7.542,3.125c1.375,0,2.771-0.26,4.083-0.813c3.979-1.646,6.583-5.542,6.583-9.854v-32 c0-2.833-1.125-5.542-3.125-7.542l-13.792-13.792h155.583v155.583l-13.792-13.792c-2-2-4.708-3.125-7.542-3.125h-32 c-4.313,0-8.208,2.594-9.854,6.583c-1.646,3.99-0.729,8.573,2.313,11.625l74.667,74.667c2.083,2.083,4.813,3.125,7.542,3.125 c2.729,0,5.458-1.042,7.542-3.125l74.667-74.667c3.042-3.052,3.958-7.635,2.313-11.625c-1.646-3.99-5.542-6.583-9.854-6.583h-32 c-2.833,0-5.542,1.125-7.542,3.125l-13.792,13.792V277.333h155.583l-13.792,13.792c-2,2-3.125,4.708-3.125,7.542v32 c0,4.313,2.604,8.208,6.583,9.854c1.313,0.552,2.708,0.813,4.083,0.813c2.771,0,5.5-1.083,7.542-3.125l74.667-74.667 C513.042,259.375,513.042,252.625,508.875,248.458z"/> </g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> </svg>';
                            break;

                case 'square':
                    $output ='<?xml version="1.0" encoding="iso-8859-1"?> <!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  --> <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 384 384" style="enable-background:new 0 0 384 384;" xml:space="preserve"> <g> <g> <path d="M341.333,0H42.667C19.093,0,0,19.093,0,42.667v298.667C0,364.907,19.093,384,42.667,384h298.667 C364.907,384,384,364.907,384,341.333V42.667C384,19.093,364.907,0,341.333,0z M341.333,341.333H42.667V42.667h298.667V341.333z" /> </g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> </svg>';
                            break;

                default:
                $output = '';
                break;
        }

        return $output;
    }
}

/**
 * Isobar SVG class
 *
 * @return  void
 */
if ( ! function_exists('dev_svg') ) {
    function dev_svg( $icon = null ){
        echo dev_get_svg( $icon );
    }
}