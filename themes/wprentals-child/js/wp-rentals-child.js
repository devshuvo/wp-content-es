;(function($){
    'use strict'

    $(document).ready(function(){

        jQuery('input[type="checkbox"]').each(function () {
           $(this).attr('data-value', $(this).val() );
        });

        jQuery(document).on('click', 'input[type="checkbox"]', function () {

            jQuery('input[type="checkbox"]').each(function () {
               $(this).val( $(this).attr('data-value') );
            });
        });

        // Change to trigger ajax
        jQuery(document).on('change', '.top-search-div select', function () {

           wpestate_start_filtering_ajax_map();
        });

        jQuery(document).on('change', '.checklist-selection input', function () {
           wpestate_start_filtering_ajax_map();
        });

        jQuery(document).on('keyup', '#search_by_name, #search_by_id', function () {

           wpestate_start_filtering_ajax_map();
        });

        $("#prop_category_submit").select2({
            maximumSelectionLength: 5,
            placeholder: "Select a Localización",
        });

        $("#prop_action_category_submit").select2({
            placeholder: "Select a Listado",
        });

        $("#prop_action_estilos_submit").select2({
            placeholder: "Select a Estilos",
        });

        $("#prop_provincia_submit").select2({
            placeholder: "Select Provincia",
            width: '100%',
        });

        $( document ).on('click', ".adv_search_toggle", function(){
            $(this).parent().toggleClass('open').find('.advanced_search_map_list_container').slideToggle();
        });

        $('div[data-type="range"]').each(function(){
            var $this = $(this);

            var low_val = $this.data('min');
            var max_val = $this.data('max');
            var now_val = '';
            var measure = '';

            $this.slider({
                range: true,
                min: parseFloat(low_val),
                max: parseFloat(max_val),
               // value: parseFloat(now_val),
                range: "max",
                slide: function (event, ui) {

                    $this.parent().find('input').val( ui.value);
                    $this.parent().find('.range_value').text(ui.value+" "+measure);

                },
                stop: function (event, ui) {
                    wpestate_start_filtering_ajax_map();
                }
            });
        });

        if ( $(document.body).hasClass('page-template-advanced_search_results') ) {
            wpestate_start_filtering_ajax_map();
        }

    });

    $(window).load(function(){
        //wpestate_start_filtering_ajax_map();
    });

})(jQuery)