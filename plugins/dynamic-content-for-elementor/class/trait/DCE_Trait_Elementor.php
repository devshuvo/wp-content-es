<?php

namespace DynamicContentForElementor;

/**
 * Description of DCE_Trait_Plugin
 *
 */
trait DCE_Trait_Elementor {

    public static $documents = [];

    // ************************************** ELEMENTOR ***************************/
    public static function get_all_template($def = null) {

        $type_template = array('elementor_library', 'oceanwp_library');

        // Return elementor templates array

        if ($def) {
            $templates[0] = 'Default';
            $templates[1] = 'NO';
        } else {
            $templates[0] = 'NO';
        }

        $get_templates = self::get_templates(); //get_posts(array('post_type' => $type_template, 'numberposts' => -1, 'post_status' => 'publish', 'orderby' => 'title', 'order' => 'DESC', 'suppress_filters' => false ));
        //print_r($get_templates);
        if (!empty($get_templates)) {
            foreach ($get_templates as $template) {
                $templates[$template['template_id']] = $template['title'] . ' (' . $template['type'] . ')';
                //$options[ $template['template_id'] ] = $template['title'] . ' (' . $template['type'] . ')';
                //$types[ $template['template_id'] ] = $template['type'];
            }
        }

        return $templates;
    }

    public static function get_templates() {
        return \Elementor\Plugin::instance()->templates_manager->get_source('local')->get_items([
                    'type' => ['section', 'archive', 'page', 'single'],
        ]);
    }

    public static function get_element_by_id($element_id = null, $post_id = null) {
        $settings = array();
        if (!$post_id) {
            $post_id = get_the_ID();
            if (!$post_id && isset($_GET['post'])) {
                $post_id = $_GET['post'];
            }
            if (!$post_id && isset($_POST['post_id'])) {
                $post_id = $_POST['post_id'];
            }
        }

        if ($post_id) {
            $elementor_data = self::get_elementor_data($post_id, '_elementor_data', true);
            if ($elementor_data) {
                if ($element_id) {
                    $element = self::array_find_deep_value($elementor_data, $element_id, 'id');
                    if (isset($element['id'])) {
                        return $element;
                    }
                }
                //return $elementor_data;
            }
        }

        $ext_post_id = self::get_post_id_by_element_id($element_id);
        if ($ext_post_id) {
            $element = self::get_element_by_id($element_id, $ext_post_id);
            if ($element) {
                return $element;
            }
        }

        return false;
    }
    
    public static function get_post_id_by_element_id($element_id) {
        $ext_post_id = false;
        if (isset(self::$documents[$element_id])) {
            $ext_post_id = self::$documents[$element_id];
        } else {
            // find element settings (because it may not be on post, but in a template)
            global $wpdb;
            $table = $wpdb->prefix . 'postmeta';
            //$query = "SELECT post_id FROM " . $table . " WHERE meta_value LIKE '%[{\"id\":\"".$element_id."\",%'";
            $query = "SELECT post_id FROM " . $table . " WHERE meta_key LIKE '_elementor_data' AND meta_value LIKE '%\"id\":\"" . $element_id . "\",%'";
            //echo $query;
            $results = $wpdb->get_results($query);
            if (!empty($results)) {
                $result = reset($results);
                $post_id = reset($result);
                $ext_post_id = self::$documents[$element_id] = $post_id;
            }
        }
        return $ext_post_id;
    }
    
    public static function get_elementor_element_by_id($element_id, $post_id = null) {
        //var_dump($element_id); die();
        if (!$post_id) {
            if ($element_id) {
                $post_id = self::get_post_id_by_element_id($element_id);
            }
            if (!$post_id) {
                $post_id = get_the_ID();
                if (!$post_id && isset($_GET['post'])) {
                    $post_id = $_GET['post'];
                }
                if (!$post_id && isset($_POST['post_id'])) {
                    $post_id = $_POST['post_id'];
                }
            }
        }
        if ($post_id) {
            $document = \Elementor\Plugin::$instance->documents->get($post_id);
            if ($document) {
                $element_raw = DCE_Helper::find_element_recursive($document->get_elements_data(), $element_id);
                //$form = DCE_Helper::get_element_by_id($element_id, $post_id);
                if ($element_raw) {
                    $element = \Elementor\Plugin::$instance->elements_manager->create_element_instance($element_raw);
                    return $element;
                } else {
                    $ext_post_id = self::get_post_id_by_element_id($element_id);
                    if ($ext_post_id != $post_id) {
                        return self::get_elementor_element_by_id($element_id, $ext_post_id);
                    }
                }
            }
        }
        return false;
    }
    
    /*public static function get_elementor_element_parent($element_id = null, $post_id = null) {
        $element = self::get_elementor_element_by_id($element_id, $post_id);
        if ($element) {                    
            $settings = $element->get_settings_for_display();
            return $settings;
        }
        return false;
    }*/
    
    
    public static function get_elementor_element_settings_by_id($element_id = null, $post_id = null) {
        $element = self::get_elementor_element_by_id($element_id, $post_id);
        if ($element) {                    
            $settings = $element->get_settings_for_display();
            return $settings;
        }
        return false;
    }

    public static function get_settings_by_id($element_id = null, $post_id = null) {
        $element = self::get_element_by_id($element_id, $post_id);
        if ($element && !empty($element['settings'])) {
            return $element['settings'];
        }
        return false;
    }

    public static function get_elementor_data($post_id) {
        if ($post_id && is_numeric($post_id)) {
            $elementor_data = get_post_meta($post_id, '_elementor_data', true);
            if ($elementor_data) {
                $post_meta = json_decode($elementor_data, true);
                return $post_meta;
            }
        }
        return false;
    }

    public static function set_all_settings_by_id($element_id = null, $settings = array(), $post_id = null) {
        if (!$post_id) {
            $post_id = get_the_ID();
            if (!$post_id) {
                $post_id = $_GET['post'];
            }
        }
        $post_meta = self::get_settings_by_id(null, $post_id);
        if ($element_id) {
            $keys_array = self::array_find_deep($post_meta, $element_id);
            $tmp_key = array_search("id", $keys_array);
            if ($tmp_key !== false) {
                $keys_array[$tmp_key] = 'settings';
            }
            $post_meta = DCE_Helper::set_array_value_by_keys($post_meta, $keys_array, $settings);
            array_walk_recursive($post_meta, function($v, $k) {
                $v = self::escape_json_string($v);
            });
        }
        $post_meta_prepared = json_encode($post_meta);
        $post_meta_prepared = wp_slash($post_meta_prepared);
        update_metadata('post', $post_id, '_elementor_data', $post_meta_prepared);
    }

    public static function set_settings_by_id($element_id, $key, $value = null, $post_id = null) {
        if (!$post_id) {
            $post_id = get_the_ID();
            if (!$post_id) {
                $post_id = $_GET['post'];
            }
        }
        $post_meta = self::get_elementor_data($post_id);
        $keys_array = self::array_find_deep($post_meta, $element_id);
        if (!empty($keys_array)) {
            $tmp_key = array_search("id", $keys_array);
            if ($tmp_key !== false) {
                array_pop($keys_array);
                $keys_array[] = 'settings';
            }
            $keys_array[] = $key;
            //var_dump($post_meta);
            //var_dump($keys_array); die();
            $post_meta = DCE_Helper::set_array_value_by_keys($post_meta, $keys_array, $value);
            array_walk_recursive($post_meta, function($v, $k) {
                $v = self::escape_json_string($v);
            });
            $post_meta_prepared = json_encode($post_meta);
            $post_meta_prepared = wp_slash($post_meta_prepared);
            update_metadata('post', $post_id, '_elementor_data', $post_meta_prepared);
        }
        return $post_id;
    }

    public static function set_dynamic_tag($editor_data) {
        if (is_array($editor_data)) {
            foreach ($editor_data as $key => $avalue) {
                $editor_data[$key] = self::set_dynamic_tag($avalue);
            }
            if (isset($editor_data['elType'])) {
                foreach ($editor_data['settings'] as $skey => $avalue) {
                    //if ($editor_data['type'] == 'text' || $editor_data['type'] == 'textarea') {
                    $editor_data['settings'][\Elementor\Core\DynamicTags\Manager::DYNAMIC_SETTING_KEY][$skey] = 'token';
                }
            }
        }
        return $editor_data;
    }

    public static function recursive_array_search($needle, $haystack, $currentKey = '') {
        foreach ($haystack as $key => $value) {
            if (is_array($value)) {
                $nextKey = self::recursive_array_search($needle, $value, is_numeric($key) ? $currentKey . '[' . $key . ']' : $currentKey . '["' . $key . '"]');
                if ($nextKey) {
                    return $nextKey;
                }
            } else if ($value == $needle) {
                return is_numeric($key) ? $currentKey . '[' . $key . ']' : $currentKey . '["' . $key . '"]';
            }
        }
        return false;
    }

    public static function find_element_recursive($elements, $element_id) {
        foreach ($elements as $element) {
            if ($element_id === $element['id']) {
                return $element;
            }

            if (!empty($element['elements'])) {
                $element = self::find_element_recursive($element['elements'], $element_id);

                if ($element) {
                    return $element;
                }
            }
        }
        return false;
    }

    public static function array_find_deep($array, $search, $keys = array()) {
        if (!empty($array)) {
            foreach ($array as $key => $value) {
                if (is_array($value)) {
                    $sub = self::array_find_deep($value, $search, array_merge($keys, array($key)));
                    if (count($sub)) {
                        return $sub;
                    }
                } elseif ($value === $search) {
                    return array_merge($keys, array($key));
                }
            }
        }
        return array();
    }

    public static function array_find_deep_value($array, $value, $key) {
        if (is_array($array)) {
            foreach ($array as $akey => $avalue) {
                if (is_array($avalue)) {
                    if (isset($avalue[$key]) && $value == $avalue[$key]) {
                        return $avalue;
                    }
                    $sub = self::array_find_deep_value($avalue, $value, $key);
                    if (!empty($sub)) {
                        return $sub;
                    }
                }
            }
        }
        return false;
    }

    // @P mod
    public static function get_the_id($datasource = false, $fromparent = false) {

        global $product;
        global $post;
        global $paged;

        // BACKUP
        $original_post = $post;
        $original_product = $product;
        $original_paged = $paged;

        $id_page = get_the_ID();

        if ($datasource) {
            $id_page = $datasource;
        }

        if ($id_page && $fromparent) {
            $the_parent = wp_get_post_parent_id($id_page);
            if ($the_parent != 0) {
                $id_page = $the_parent;
            }
        }

        if (!$id_page) {
            global $wp;
            $current_url = home_url(add_query_arg(array(), $wp->request));
            $id_page = url_to_postid($current_url);
        }

        // 1) ME-STESSO (naturale)
        //var_dump($id_page);
        $type_page = get_post_type($id_page);
        $id_page = self::get_rev_ID($id_page, $type_page);
        //var_dump($id_page);
        // DEMO
        if (\Elementor\Plugin::$instance->editor->is_edit_mode()) {
            $demoPage = get_post_meta(get_the_ID(), 'demo_id', true); // using get_the_id to retrive Template ID
            if ($demoPage) {
                $id_page = $demoPage;
                $product = self::wooc_data($id_page); //wc_get_product( $id_page );
                $post = get_post($id_page);
            }
        }

        // RESET
        $post = $original_post;
        if ($type_page != 'product')
            $product = $original_product;
        $paged = $original_paged;

        return $id_page;
    }

    public static function is_edit_mode() {
        return \Elementor\Plugin::$instance->editor->is_edit_mode() || isset($_GET['elementor-preview']);
    }

    public static function get_template_id_by_html($content = '') {
        $tmp = explode('elementor elementor-', $content, 2);
        if (count($tmp) > 1) {
            $tmp = str_replace('"', ' ', end($tmp));
            $tmp = explode(' ', $tmp, 2);
            if (count($tmp) > 1) {
                $tmp = reset($tmp);
                return intval($tmp);
            }
        }
        return false;
    }

    public static function get_theme_builder_template_id($location = false) {
        $template_id = 0;
        if (DCE_Helper::is_plugin_active('elementor-pro')) {
            // check with Elementor PRO Theme Builder
            if (!$location) {
                if (is_singular()) {
                    $location = 'single';
                } else {
                    $location = 'archive';
                }
            }
            //if (is_archive()) { $location = 'archive'; }
            $document = \ElementorPro\Modules\ThemeBuilder\Module::instance()->get_conditions_manager()->get_documents_for_location($location);
            if (!empty($document)) {
                $document = reset($document);
                //echo '<pre>'; var_dump($document); echo '</pre>'; die();
                //var_dump(get_class($document)); die();
                $template_id = $document->get_main_id();
            }
        }
        return $template_id;
    }

}
