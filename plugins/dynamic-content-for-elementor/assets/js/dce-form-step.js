function dce_show_step(target, element, direction = 'next') {
    var step = jQuery('.elementor-element-' + element + ' #dce-form-step-' + target);
    jQuery('.elementor-element-' + element + ' .elementor-error').removeClass('elementor-error');
    jQuery('.elementor-element-' + element + ' .dce-form-step').hide();
    jQuery('.elementor-element-' + element + ' .dce-step-active').removeClass('dce-step-active');

    jQuery('.elementor-element-' + element + ' .dce-step-active-progressbar').removeClass('dce-step-active-progressbar');
    step.show().addClass('dce-step-active');
    jQuery('.elementor-element-' + element + ' #dce-form-step-' + target + '-progressbar').addClass('dce-step-active-progressbar');
    
    jQuery('.elementor-element-' + element + ' .dce-step-active-summary').removeClass('dce-step-active-summary');
    jQuery('.elementor-element-' + element + ' #dce-form-step-' + target + '-summary').addClass('dce-step-active-summary');
    jQuery('.elementor-element-' + element + ' #dce-form-step-' + target + '-summary').addClass('dce-step-filled-summary');

    if (step.hasClass('dce-form-visibility-step')) {
        if ((step.hasClass('dce-form-visibility-step-hide-init') && !step.hasClass('dce-form-visibility-step-hide'))
                || (step.hasClass('dce-form-visibility-step-show-init') && !step.hasClass('dce-form-visibility-step-show'))) {
            console.log('skip step ' + target);
            var new_target = step.find('.elementor-button-' + direction).attr('data-target');
            dce_show_step(new_target, element);
        }
    }
}
function dce_validate_step(step) {
    var next = true;
    step.find('.elementor-field-required input, .elementor-field-required select, .elementor-field-required textarea').each(function () {
        if (jQuery(this).prop('required') && !jQuery(this).prop('disabled')) {
            switch (jQuery(this).attr('type')) {
                case 'checkbox':
                //case 'acceptance':
                case 'radio':
                    var tmp = jQuery(this).attr('id').split('-');
                    tmp.pop();
                    var base_id = tmp.join('-');
                    if (!jQuery('input[id^="' + base_id + '"]:checked').length) {
                        next = false;
                    }
                    break;
                default:
                    if (!jQuery(this).val() || !jQuery(this).is(':valid')) {
                        next = false;
                    }
            }
        }
        if (!next) {
            //console.log(this);
            jQuery(this).closest('.elementor-field-required').addClass('elementor-error');
        }
    });
    return next;
}