<?php
$adv_submit             =   wpestate_get_template_link('advanced_search_results.php');
$guest_list             =   wpestate_get_guest_dropdown();
$local_args_search_map_list = wpestate_get_select_arguments();
$allowed_html = array();
$allowed_html_list =    array('li' => array(
                                        'data-value'        =>array(),
                                        'role'              => array(),
                                        'data-parentcity'   =>array(),
                                        'data-value2'       =>array()
                        ) );
$action_select_list =   wpestate_get_action_select_list($local_args_search_map_list);
$categ_select_list  =   wpestate_get_category_select_list($local_args_search_map_list);
$select_city_list   =   wpestate_get_city_select_list($local_args_search_map_list);
$select_area_list   =   wpestate_get_area_select_list($local_args_search_map_list);
$select_county_state_list = array();
$min_price_slider   =   floatval(wprentals_get_option('wp_estate_show_slider_min_price',''));
$max_price_slider   =   floatval(wprentals_get_option('wp_estate_show_slider_max_price',''));
$wpestate_where_currency     =   esc_html( wprentals_get_option('wp_estate_where_currency_symbol', '') );
$wpestate_currency  =   esc_html( wprentals_get_option('wp_estate_currency_label_main', '') );

if(isset($_GET['price_max'])){
    $max_price_slider = floatval($_GET['price_max']);
}

if(isset($_GET['price_low'])){
    $min_price_slider = floatval($_GET['price_low']);
}


if ($wpestate_where_currency == 'before') {
    $price_slider_label = esc_html($wpestate_currency) . number_format($min_price_slider).' '.esc_html__( 'to','wprentals').' '.esc_html($wpestate_currency) . number_format($max_price_slider);
}else {
    $price_slider_label =  number_format($min_price_slider).esc_html($wpestate_currency).' '.esc_html__( 'to','wprentals').' '.number_format($max_price_slider).esc_html($wpestate_currency);
}

?>

<div id="advanced_search_map_list">
    <button type="text" class="adv_search_toggle">
        <svg class="normal" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="18px" height="18px"><path d="M7 10l5 5 5-5z"/><path d="M0 0h24v24H0z" fill="none"/></svg>
        <svg class="active" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="black" width="18px" height="18px"><path d="M7 14l5-5 5 5z"/><path d="M0 0h24v24H0z" fill="none"/></svg>
        <?php esc_html_e('Filtros','wprentals');?>
    </button>
    <div class="advanced_search_map_list_container">
        <?php

        if( wprentals_get_option('wp_estate_use_geo_location','')=='yes'){

        $radius_measure = wprentals_get_option('wp_estate_geo_radius_measure','');
        $radius_value   = wprentals_get_option('wp_estate_initial_radius','');

        ?>
            <div class="col-md-12 radius_wrap" style="display: none;">
                <input type="text" id="geolocation_search" class="form-control" name="geolocation_search" placeholder="<?php esc_html_e('Location','wprentals');?>" value="">
                <input type="hidden" id="geolocation_lat" name="geolocation_lat">
                <input type="hidden" id="geolocation_long" name="geolocation_lat">
            </div>

            <div class="col-md-12 full-padding">
                <div class="row">
                    <div class="col-md-6">
                        <label for="search_by_name"><?php esc_html_e('Search by name:','wprentals');?></label>
                        <input type="text" id="search_by_name" class="form-control" name="search_by_name" placeholder="<?php esc_html_e('Search by name:','wprentals');?>" value="">
                    </div>

                    <div class="col-md-6">
                        <label for="search_by_id"><?php esc_html_e('Search by Id:','wprentals');?></label>
                        <input type="number" id="search_by_id" class="form-control" name="search_by_id" placeholder="<?php esc_html_e('Search by Id:','wprentals');?>" value="">
                    </div>

                    <div class="col-md-6">
                        <div class="custom-ui-slider-wrap">
                            <label for="capacidad"><?php esc_html_e('CAPACIDAD:','wprentals');?> <span class="range_value"></span></label>
                            <div class="custom-ui-slider" data-type="range" data-max="1000" data-min="0" ></div>
                            <input type="hidden" id="capacidad" class="form-control" name="capacidad" placeholder="<?php esc_html_e('CAPACIDAD:','wprentals');?>" value="">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="custom-ui-slider-wrap">
                            <label for="superficie"><?php esc_html_e('SUPERFICIE:','wprentals');?> <span class="range_value"></span></label>
                            <div class="custom-ui-slider" data-type="range" data-max="1000" data-min="0" ></div>
                            <input type="hidden" id="superficie" class="form-control" name="superficie" placeholder="<?php esc_html_e('SUPERFICIE:','wprentals');?>" value="">
                        </div>
                        </div>

                    <div class="col-md-6">
                        <div class="custom-ui-slider-wrap">
                            <label for="precio_hora"><?php esc_html_e('PRECIO/HORA:','wprentals');?> <span class="range_value"></span></label>
                            <div class="custom-ui-slider" data-type="range" data-max="1000" data-min="0" ></div>
                            <input type="hidden" id="precio_hora" class="form-control" name="precio_hora" placeholder="<?php esc_html_e('PRECIO/HORA:','wprentals');?>" value="">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="custom-ui-slider-wrap">
                            <label for="salas"><?php esc_html_e('SALAS:','wprentals');?> <span class="range_value"></span></label>
                            <div class="custom-ui-slider" data-type="range" data-max="1000" data-min="0" ></div>
                            <input type="hidden" id="salas" class="form-control" name="salas" placeholder="<?php esc_html_e('SALAS:','wprentals');?>" value="">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <label><?php esc_html_e('SERVICIOS:','wprentals');?></label>
                        <ul class="checklist-selection">
                            <?php
                            $args = array(
                                'taxonomy'             => 'servicios',
                            );
                            dev_terms_checklist( $post_id = 0, $args ); ?>
                        </ul>
                    </div>

                    <div class="col-md-6">
                        <label><?php esc_html_e('NORMAS:','wprentals');?></label>
                        <ul class="checklist-selection">
                            <?php
                            $args = array(
                                'taxonomy'             => 'normas',
                            );
                            dev_terms_checklist( $post_id = 0, $args ); ?>
                        </ul>
                    </div>

                    <div class="col-md-6">
                        <label><?php esc_html_e('ESTILOS:','wprentals');?></label>
                        <ul class="checklist-selection">
                            <?php
                            $args = array(
                                'taxonomy'             => 'estilos',
                            );
                            dev_terms_checklist( $post_id = 0, $args ); ?>
                        </ul>
                    </div>

                </div>
            </div>

            <div class="col-md-3 slider_radius_wrap hidden">
                <div class="label_radius"><?php esc_html_e('Radius:','wprentals');?> <span class="radius_value"><?php print esc_html($radius_value.' '.$radius_measure);?></span></div>
            </div>

            <div class="col-md-9 slider_radius_wrap hidden">
                <div id="wpestate_slider_radius"></div>
                <input type="hidden" id="geolocation_radius" name="geolocation_radius" value="<?php print esc_html($radius_value);?>">
            </div>
        <?php
        }
        ?>

    </div>
</div>


<!--<div id="advanced_search_map_list_hidden">
    <div class="col-md-2">
        <div class="show_filters" id="adv_extended_options_show_filters"><?php esc_html_e('Search Options','wprentals')?></div>
    </div>
</div>-->